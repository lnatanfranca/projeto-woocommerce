<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OOwC9pxIWlEjavwmwNMUhEZ6JL7ytC4V3foUAldeRzpar8gQelvZT1U9I6zwTZLlaOGdGSwB5Cy3mZMCfu0Ulw==');
define('SECURE_AUTH_KEY',  'Kv8HATRTbygm/v8ARqE4YzCCVrT6IExrinIr2idxQG24112Y4c+91ZpEM1uILAY3kqh1uwQLW2WE6O9xX0im4g==');
define('LOGGED_IN_KEY',    'D4REcGL4+JiqpPkq81w+cNd3g9RztUNIiU1NI43JYqYokbH81nsn46WHWKGYz3ipCZi1j8u7oJClbClFcZZqcA==');
define('NONCE_KEY',        'MQ/eeTecn7MPwozPp/it2gpe3JC2Sa27Hmut6d80lMlOaGCcmMatLbSKOXG96c9T3THAujeX7lOyEEqXsOOxdw==');
define('AUTH_SALT',        'LiQmjUdhEO63N+q22Xx5zs/+OyicvKJqn/LPHL6a5DwPIoYTSVluVLNZAty1vEzxDxmcFsQW+TZACezFXXJFQg==');
define('SECURE_AUTH_SALT', '8IT+zFkZ8vlsqLD0X0CmGyYesoxLWeJLFaV+5Y8bgZphIkWvlKMFiieROOn3CxywpCgk8HAVwcVlFGu/gcOSAA==');
define('LOGGED_IN_SALT',   'GzoFKmF/SZdoajD/0LDj4u0SlxfXREeVm1YNY/owp+hCuy1bq/wVUxYeUym2kIpcN8mrIueEGvKr0hXJpnkVQA==');
define('NONCE_SALT',       '1PydXqYpluVgAIYRdG4l23CbZa6hlP7TBI4i40PpldkDVa5gpHxpxrR+Z6Pwuq7yI4iXe+ya3+K4gPRgTN0PUw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
