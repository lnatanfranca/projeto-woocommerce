<?php 

get_header(); 
// Template name: Front Page

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php endwhile; else : ?>
<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif;

// Filtra os pratos de acordo com o dia da semana

$dishes_front = wc_get_products([
    'limit' => 4,
    'tag' => [apply_filters('dia_da_semana', date('w'))]
]);

$dayweek_dishes = filter_products($dishes_front); 

?>


<main class="front-main">
    <section class="front-header">
        <h1>Comes&Bebes</h1>
        <p>O restaurante para todas as fomes</p>
    </section>
    <section class="middle-main">
        <h1>CONHEÇA NOSSA LOJA</h1>
        <section class="pratos-principais">
            <span>Tipos de pratos principais</span>
            <?php do_action('mostra_categorias') ?>
        </section>
        <section class="pratos-do-dia">
            <div class="pratos-do-dia-header">
                <span>Pratos do dia de hoje</span>
                <span><?php echo apply_filters('dia_da_semana', date('w'))?></span>
            </div>
            <?php do_action('dayweek_dishes', $dayweek_dishes)?>
            <a class="other-options-btn" href="<?php echo get_page_link( get_page_by_title('Lista de produtos')->ID ); ?>">
                <button class="button">Veja outras opções</button>
            </a>
        </section>
    </section>
    <section class="bottom-main">
        <h1>VISITE NOSSA LOJA FÍSICA</h1>
        <div class="bottom-main-body">
            <div class="contact-us">
                <div class="contact-text">
                    <iframe width="300" height="225" style="border:0" loading="lazy" allowfullscreen src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJK2lORH6BmQAR9fJ6V_s1Xv0&key=AIzaSyBjqMWIiZ_yM8GRPzFPSzIdcqjHcV6RbtE"></iframe>
                    <div class="address">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/address.png" alt="">
                        <span>Av. Gal. Milton Tavares de Souza, Niterói - RJ</span>
                    </div>
                    <div class="phone">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/phone.png" alt="">
                        <span>(XX) XXXX-XXXX</span>
                    </div>
                </div>
            </div>
            <img class="store-img" src="<?php echo get_stylesheet_directory_uri() ?>/img/gente-comendo.png" alt="">
        </div>
    </section>
</main>

<?php get_footer(); 

?>