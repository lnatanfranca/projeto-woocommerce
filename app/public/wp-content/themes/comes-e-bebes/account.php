<?php get_header(); 
// Template name: Account
?>

<main class="minha-conta-main">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <h1 class="hidden"><?php the_title(); ?></h1>
        <p class="hidden"><?php the_content(); ?></p>
    <?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
    <?php endif; ?>
</main>

<?php get_footer(); 

?>