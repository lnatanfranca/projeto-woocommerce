<?php 

function comesbebes_add_woocommerce_support() {
    add_theme_support('woocommerce');
    add_theme_support('menus');
}

add_action('after_setup_theme', 'comesbebes_add_woocommerce_support');

function comesbebes_css() {
    wp_register_style('comesbebes-style', get_template_directory_uri() . '/style.css', [], '1.0.0', false);
    wp_enqueue_style('comesbebes-style');
}

add_action('wp_enqueue_scripts', 'comesbebes_css');

// Função para retornar o dia da semana em português

function get_dayweek_portuguese($dia) {
    if ($dia == '0') return 'DOMINGO';
    else if ($dia == '1') return 'SEGUNDA';
    else if ($dia == '2') return 'TERÇA';
    else if ($dia == '3') return 'QUARTA';
    else if ($dia == '4') return 'QUINTA';
    else if ($dia == '5') return 'SEXTA';
    else if ($dia == '6') return 'SÁBADO';
}

add_filter('dia_da_semana', 'get_dayweek_portuguese');

// Função para filtrar as informações de cada prato

function filter_products($products, $img_size = 'large') {
    $final_products = [];

    foreach($products as $product) {
        $final_products[] = [
            'name' => $product->get_name(),
            'price' => $product->get_price_html(),
            'link' => $product->get_permalink(),
            'img' => wp_get_attachment_image_src( $product->get_image_id(), $img_size)[0]
        ];
    }

    return $final_products;
}

// Função para mostrar os pratos na página principal

function format_products_container($products) { ?>
    <ul class="dishes-container">
    <?php foreach($products as $product) { ?>
        <li class="dish-container">
            <img class="dish-img" src="<?= $product['img']?>" alt="foto de <?= $product['name'] ?>">
            <div class="dish-info">
                <span class="dish-name"><?= $product['name']?></span>
                <span class="dish-price"><?= $product['price']?></span>
                <a class="buy-dish" href="<?= $product['link']?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/buy-icon.png" alt="botão de compra"></a>
            </div>
        </li>
    <?php } ?>
    </ul>
<?php
}

add_action('dayweek_dishes', 'format_products_container');

function create_cat_menu(){
    wp_nav_menu([
        'menu' => 'categorias',
        'container' => 'ul',
        'container_class' => 'menu-categorias',
        'before' => '<img src="" alt="">'
    ]);
}

// Junta página de edição da conta com a página da conta

function add_edit_form($user){?>
    <form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr($user->first_name); ?>" />
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr($user->last_name); ?>" />
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_display_name"><?php esc_html_e( 'Display name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_display_name" id="account_display_name" value="<?php echo esc_attr($user->display_name); ?>" /> <span><em><?php esc_html_e( 'This will be how your name will be displayed in the account section and in reviews', 'woocommerce' ); ?></em></span>
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr($user->email); ?>" />
	</p>

	<fieldset>
		<legend><?php esc_html_e( 'Password change', 'woocommerce' ); ?></legend>

		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" autocomplete="off" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" autocomplete="off" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" autocomplete="off" />
		</p>
	</fieldset>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<p>
		<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
		<button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
		<input type="hidden" name="action" value="save_account_details" />
	</p>
</form>
<?php
}

add_action( 'woocommerce_account_content', 'add_edit_form');

function add_price_to_form($dish){?>
	<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>"><?php echo $dish->get_price_html(); ?></p>
<?php
}

add_action( 'woocommerce_before_variations_form', 'add_price_to_form');

add_action('comes_bebes_after_single_product_summary', 'format_products_container');

function filter_cats($cat_arr){
	$format_cat_arr = [];

	foreach ($cat_arr as $cat){
		$cat_id = $cat->term_id;
		if ($cat->slug != 'sem-categoria'){
			$thumb_id = get_term_meta($cat_id, 'thumbnail_id', true);
			$format_cat_arr[] = [
				'name' => $cat->name,
				'img' => wp_get_attachment_url($thumb_id),
				'slug' => $cat->slug
			];
		}
	}

	return $format_cat_arr;
}

function format_categories(){
	$dish_categories = get_categories([
		'taxonomy'     => 'product_cat',
		'orderby'      => 'name',
		'show_count'   => 0,
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => 0
	]);
	
	$cat_arr = filter_cats($dish_categories);
	
	?>
	<ul class="cats-container">
		<?php foreach($cat_arr as $cat) {?>
			<li class="cat-container">
				<img class="cat-img" src="<?= $cat['img'] ?>" alt="comida <?= strtolower($cat['name'])?>">
				<form class="txt-container" action="<?php bloginfo('url');?>/lista-de-produtos/" method="get">
					<input type="text" name="category" value="<?= $cat['slug']?>" class="hidden">
					<button class="cat-button" type="submit">
						<h1 class="cat-title"><?= $cat['name'] ?></h1>
					<button>
				</form>
			</li>
			<?php } ?>
    </ul>
<?php
}

add_action('mostra_categorias', 'format_categories');