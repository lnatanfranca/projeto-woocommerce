<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;700&display=swap" rel="stylesheet">
    <title><?php the_title() ?></title>
    <?php wp_head(); ?>
</head>
<body class="body">
    <header class="header">
        <div class="left-header">
            <a href="<?php echo get_page_link( get_page_by_title('Página principal')->ID ); ?>">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/icone-header.png" alt="">
            </a>
            <div class="search">
                <form action="<?php bloginfo('url');?>/lista-de-produtos/" method="get">
                    <input type="text" name="post-type" value="product" class="hidden">
                    <input class="search-field" type="text" name="s" id="s" value="<?php the_search_query(); ?>">
                    <input type="image" src="<?php echo get_stylesheet_directory_uri() ?>/img/search-icon.png" name="s" id="s">
                </form>
            </div>
        </div>
        <div class="right-header">
            <a href="<?php echo get_page_link( get_page_by_title('Lista de produtos')->ID ); ?>">
                <button class="button">Faça um pedido</button>
            </a>
            <a href="">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/shopping-cart.png" alt="" >
            </a>
            <a href="<?php echo get_page_link( get_page_by_title('Minha conta')->ID ); ?>">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/user.png" alt="" >
            </a>
        </div>
    </header>
